<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="bundle.js" type="application/javascript"></script>
    <title>Unofficial RSS Feeds for Stitcher Premium</title>
    @if (env('GA_TRACKING_ID'))
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', '{{env('GA_TRACKING_ID')}}', 'auto');
    ga('send', 'pageview');
    </script>
    @endif
</head>
<body>
    <nav id="navbar" class="navbar is-light">
        <div class="container">
            <div class="navbar-brand logged-out-brand has-text-centered">
                <a class="navbar-item has-text-weight-bold" href="/">
                    Unofficial RSS Feeds<br>for Stitcher Premium
                </a>
            </div>


            <!-- Desktop -->
            <div class="navbar-end is-hidden-touch">
                <div class="navbar-item">
                </div>
            </div>
        </div>
    </nav>

    <section class="section">
        @yield('content')
    </section>

    <section class="section has-text-centered">
        Made with ❤ by <a href="https://128.io">John Long</a>
        <br>
        <a href="{{ env('SRC_URL') }}">source</a>
        <span class="has-text-grey-lighter">&dash;</span>
        <a href="https://paypal.me/adduc">paypal</a>
        <span class="has-text-grey-lighter">&dash;</span>
        <a href="https://venmo.com/adduc">venmo</a>
    </section>
</body>
</html>
